# this file automates s3 and dynamodb resources management, helping avoiding state conflicts

provider "aws" {
  region = "us-east-1"
}

module "bootstrap" {
  source = "trussworks/bootstrap/aws"
  region = "us-east-1"
  version = "5.2.0"
  account_alias = "volvo-cars-account"
}
