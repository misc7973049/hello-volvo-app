stages:
  - build-app
  - distribute
  - tf-backend-bootstrap
  - eks-init-and-plan
  - eks-apply
  - deploy-to-eks
  - eks-destroy

cache:
  paths:
    - .terraform/
    - .terraform.lock.hcl

build-app:
  tags:
    - node
  stage: build-app
  variables:
    ORG: renatofenrir
    IMAGE_NAME: hello-volvo-app
    IMAGE_TAG: v1
    ENDPOINT: "localhost:8080"
  script:
    - echo "[INFO] Removing previous testing containers if any"
    - export CONTAINER_ID=$(docker ps |grep volv |awk '{print $1}') && if [ -n "$CONTAINER_ID" ]; then docker rm -f $CONTAINER_ID; fi
    - docker build -t $ORG/$IMAGE_NAME:$IMAGE_TAG .
    - docker run --rm --name hello-volvo-app -d -p 8080:8080 --rm $ORG/$IMAGE_NAME:$IMAGE_TAG
    - export CONTAINER_ID=$(docker ps |grep volv |awk '{print $1}')
    - echo "[INFO] Checking application endpoint from container $CONTAINER_ID" && sleep 10
    - docker exec $CONTAINER_ID curl http://0.0.0.0:8080

distribute-dockerhub:
  tags:
    - docker
  stage: distribute
  variables:
    ORG: renatofenrir
    IMAGE_NAME: hello-volvo-app
    IMAGE_TAG: v1
  script:
    - echo "[INFO] Pushing container image $ORG/$IMAGE_NAME:IMAGE_TAG"
    - docker login --username=$DOCKER_USER --password=$DOCKER_PASS
    - docker push $ORG/$IMAGE_NAME:$IMAGE_TAG

tf-backend-bootstrap:
  tags:
    - terraform
  stage: tf-backend-bootstrap
  variables:
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
    AWS_DEFAULT_REGION: us-east-1
    bucket: volvo-cars-account-tf-state-us-east-1
    key: "volvo-cars/terraform.tfstate"
  script:
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID; aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY; aws configure set region $AWS_DEFAULT_REGION
    - aws configure list
    - docker run --rm -e AWS_DEFAULT_REGION="$AWS_DEFAULT_REGION" -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -u="1002:1002" -v $HOME/.aws:/home/gitlab-runner/.aws -v $PWD:/terraform -w="/terraform" hashicorp/terraform:1.3.8 -chdir="tf-backend-bootstrap/" init
    - docker run --rm -e AWS_DEFAULT_REGION="$AWS_DEFAULT_REGION" -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -u="1002:1002" -v $HOME/.aws:/home/gitlab-runner/.aws -v $PWD:/terraform -w="/terraform" hashicorp/terraform:1.3.8 -chdir="tf-backend-bootstrap/" plan
    - docker run --rm -e AWS_DEFAULT_REGION="$AWS_DEFAULT_REGION" -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -u="1002:1002" -v $HOME/.aws:/home/gitlab-runner/.aws -v $PWD:/terraform -w="/terraform" hashicorp/terraform:1.3.8 -chdir="tf-backend-bootstrap/" apply -auto-approve

tf-init-and-plan:
  tags:
    - terraform
  stage: eks-init-and-plan
  variables:
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
    AWS_DEFAULT_REGION: us-east-1
    bucket: volvo-cars-account-tf-state-us-east-1
    key: "volvo-cars/terraform.tfstate"
  script:
    - env
    - echo "[INFO] Initialising Terraform Remote Backend On $bucket S3 Bucket"
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID; aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY; aws configure set region $AWS_DEFAULT_REGION
    - aws configure list
    - docker run --rm -e AWS_DEFAULT_REGION="$AWS_DEFAULT_REGION" -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -u="1002:1002" -v $HOME/.aws:/home/gitlab-runner/.aws -v $PWD:/terraform -w="/terraform" hashicorp/terraform:1.6 init -backend-config="bucket=${bucket}" -backend-config="key=${key}" -backend-config="region=${AWS_DEFAULT_REGION}"
    - docker run --rm -e AWS_DEFAULT_REGION="$AWS_DEFAULT_REGION" -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -u="1002:1002" -v $HOME/.aws:/home/gitlab-runner/.aws -v $PWD:/terraform -w="/terraform" hashicorp/terraform:1.6 plan

tf-apply:
  tags:
    - terraform
  stage: eks-apply
  variables:
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
    AWS_DEFAULT_REGION: us-east-1
    bucket: volvo-cars-account-tf-state-us-east-1
    key: "volvo-cars/terraform.tfstate"
    cluster_name: "volvo-cars-cluster"
  script:
    - echo "[INFO] Initialising terraform apply - please check the output below:"
    - docker run --rm -e AWS_DEFAULT_REGION="$AWS_DEFAULT_REGION" -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -u="1002:1002" -v $HOME/.aws:/home/gitlab-runner/.aws -v $PWD:/terraform -w="/terraform" hashicorp/terraform:1.6 apply -auto-approve
    - echo "[INFO] Checking cluster status.."
    - aws eks --region $AWS_DEFAULT_REGION update-kubeconfig --name $cluster_name
    - kubectl get nodes && kubectl get pods -A

deploy-to-eks:
  tags:
    - k8s
  stage: deploy-to-eks
  variables:
    variables:
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
    AWS_DEFAULT_REGION: us-east-1
    bucket: volvo-cars-account-tf-state-us-east-1
    key: "volvo-cars/terraform.tfstate"
    cluster_name: "volvo-cars-cluster"
  script:
    - echo "[INFO] Configuring k8s kubeconfig"
    - aws eks --region $AWS_DEFAULT_REGION update-kubeconfig --name $cluster_name && kubectl cluster-info
    - echo "[INFO] Deploying nginx ingress-controller"
    - helm repo add nginx-stable https://helm.nginx.com/stable && helm repo update && helm install nginx-ingress nginx-stable/nginx-ingress --set rbac.create=true
    - echo "[INFO] Deploying new helm release.."
    - kubectl create ns volvo-cars && kubectl create -f ./k8s/
    - echo "[INFO] Please, check the hello-volvo-cars application endpoint below:"
    - sleep 20 && kubectl get svc -n volvo-cars --no-headers | awk '{print $4 ":8080"}'

tf-destroy:
  tags:
    - terraform
  stage: eks-destroy
  variables:
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
    AWS_DEFAULT_REGION: us-east-1
    bucket: volvo-cars-account-tf-state-us-east-1
    key: "volvo-cars/terraform.tfstate"
    namespace_name: "volvo-cars"
  script:
    - echo "[INFO] Initialising terraform destroy - please check the output below:"
    - if kubectl get namespace "$namespace_name" &> /dev/null; then kubectl delete namespace "$namespace_name"; else echo "Namespace $namespace_name does not exist. Proceeding..."; fi
    - echo "[INFO] Removing nginx-ingress-controller installation.."
    - helm delete nginx-ingress && sleep 15
    - docker run --rm -e AWS_DEFAULT_REGION="$AWS_DEFAULT_REGION" -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -u="1002:1002" -v $HOME/.aws:/home/gitlab-runner/.aws -v $PWD:/terraform -w="/terraform" hashicorp/terraform:1.6 destroy -auto-approve
  when: manual
