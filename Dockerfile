FROM node:16-alpine

RUN apk --no-cache add curl

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . ./

EXPOSE 8080
CMD [ "node", "server.js" ]