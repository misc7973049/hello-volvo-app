# hello-volvo-app

## Name
Hello-Volvo-App

## Description
This is a simple Node.js application demonstrating the exposure of a basic endpoint. The project leverages GitLab CI/CD, featuring the following jobs for streamlined application build and infrastructure provisioning:

- build-app: Utilizes Docker container runtime to build the Node.js application image.

- distribute: Publishes the Docker image to Docker Hub.

- tf-backend-bootstrap: Utilizes the trussworks/bootstrap/aws Terraform module to bootstrap the necessary infrastructure for persisting remote Terraform state files.

- eks-init-and-plan: Initializes a Terraform configuration, downloads necessary provider plugins and modules, and prepares the working directory for Terraform operations. Subsequently, it executes terraform apply to visualize the AWS infrastructure once provisioned.

- eks-apply: Creates the defined infrastructure, performs health checks on Kubernetes post-provisioning, and assesses kubeapi-server readiness.

- deploy-to-eks: Installs the nginx-ingress-controller. Once a LoadBalancer service type is created, the deployment becomes publicly accessible as it creates an ELB under the hood, pointing to the deployment running the sample app.

- tf-destroy: Destroys the entire stack.

## Installation

1. Configure AWS and Docker credentials as MASKED Gitlab CI variables; they will not be displayed on the build logs if configured this way. (NOTE: this
is a testing environment, so this setup is absolutely not recommended for prod).
2. Once they are configured, make sure that your runner is configured with the runner tags being used by each job; they are: node,docker,terraform,k8s
3. Once the above configurations are set, trigger the pipeline and wait for all the resources to be provisioned.

## Usage

Check the application endpoint that is being created by opening the 'deploy-to-eks' job; look for the log
entry 'Please, check the hello-volvo-cars application endpoint below: ..'. Open the endpoint in your browser
or curl it.

## Contributing
Suggestions are always very welcome, so please if you have any suggestions, feel free to MR.

## Authors and acknowledgment
My name is Renato Rodrigues and this is my application assignment task for joining Volvo Cars.

## License
MIT License.