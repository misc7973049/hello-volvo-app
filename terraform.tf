terraform {

  backend "s3" {
    bucket         = "volvo-cars-account-tf-state-us-east-1"
    key            = "volvo-cars/terraform.tfstate"
    dynamodb_table = "terraform-state-lock"
    region         = "us-east-1"
    encrypt        = "true"
  }
}